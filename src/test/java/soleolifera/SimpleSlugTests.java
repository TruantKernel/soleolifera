package soleolifera;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import usa.kerby.tk.soleolifera.Soleolifera;

/**
 * @author Trevor Kerby
 * @since Jul 9, 2021
 */
@TestInstance(Lifecycle.PER_CLASS)
public class SimpleSlugTests {

	Soleolifera gen;

	@BeforeAll
	public void setup() {
		this.gen = Soleolifera.create();
	}
	
	@Test
	public void shouldReturnEmptyString() {
		String given = "";
		String when = gen.generate(given);
		assertEquals("", when);
	}

	@Test
	public void shouldGenerateSlug() {
		String given = "Mr. Jock, TV quiz PhD., bags few lynx";
		String when = gen.generate(given);
		assertEquals("mr-jock-tv-quiz-phd-bags-few-lynx", when);
	}

	@Test
	public void shouldTrimAnyWhiteSpace() {
		String given = "  \tJived \nfox nymph \r\tgrabs quick \fwaltz  ";
		String when = gen.generate(given);
		assertEquals("jived-fox-nymph-grabs-quick-waltz", when);
	}

	@Test
	public void shouldNormalizeDiacritical() {
		String given = "P\u0361\u0335a\u030F\u0301c\u0323\u0353k\u032F\u0324 m\u030E\u0315y\u032D\u0355 b\u032F\u0325o\u035C\u035Fx\u034D\u0337 w\u0306\u0308i\u0316\u0356t\u031B\u0307h\u0319\u034C f\u0304\u0334i\u0309\u0332v\u0360\u031Be\u0324\u0341 d\u0300\u031Do\u0318\u034Bz\u0304\u034Ae\u0356\u0355n\u0349\u033E l\u0353\u0334i\u0357\u035Cq\u034E\u033Cu\u0357\u0355o\u033A\u0327r\u0324\u0335 j\u0317\u0315u\u0338\u0345g\u034A\u0313s\u0341\u0319";
		String when = gen.generate(given);
		assertEquals("pack-my-box-with-five-dozen-liquor-jugs", when);
	}

}
