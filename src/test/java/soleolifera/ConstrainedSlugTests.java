package soleolifera;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import usa.kerby.tk.soleolifera.Soleolifera;

/**
 * @author Trevor Kerby
 * @since Jul 9, 2021
 */
public class ConstrainedSlugTests {

	private static String fixture1 = "Congress shall make no law respecting an establishment of religion, "
			+ "or prohibiting the free exercise thereof; or abridging the freedom of speech, or of the press; "
			+ "or the right of the people peaceably to assemble, and to petition the Government for a redress of grievances.";

	private static String fixture2 = "\"embrace, extend, and extinguish is a phrase that the U.S. Department of Justice found that was used internally by Microsoft";

	private static String fixture3 = "all relevant circumstances considered The United States is a particularly exceptional experiment";

	@Test
	public void shouldInitializeProperlyWithDefaultWeights() {
		Soleolifera gen = Soleolifera.createWithDefaultWeights();
		String tested = gen.generate("Default weights without a constraint do nothing different");

		gen = Soleolifera.create();
		String expected = gen.generate("Default weights without a constraint do nothing different");

		assertEquals(expected, tested);
	}

	@Test
	public void shouldGenerateWithCharacterConstraint() {
		int given = 70;
		Soleolifera gen = Soleolifera.create().withLengthConstraint(given);
		String when = gen.generate(fixture1);
		assertTrue(when.length() <= given);
	}

	@Test
	public void shouldGenerateWeightedResultWithCharacterConstraint() {
		int given = 70;
		Soleolifera gen = Soleolifera.createWithDefaultWeights().withLengthConstraint(given);
		String when = gen.generate(fixture1);
		System.out.println(when);
		assertTrue(when.length() <= given);
		assertEquals("congress-respecting-establishment-prohibiting-government-grievances", when);
	}

	@Test
	public void shouldPreferWordsWithProperNames() {
		Soleolifera gen = Soleolifera.createWithDefaultWeights().withPreferredWordCount(4);
		String when = gen.generate(fixture2);
		System.out.println(when);
		assertTrue(when.contains("microsoft"));
	}

	@Test
	public void shouldPreferPhrasesWithCapitalizations() {
		Soleolifera gen = Soleolifera.createWithDefaultWeights().withPreferredWordCount(4);
		String when = gen.generate(fixture3);
		System.out.println(when);
		assertTrue(when.contains("the-united-states"));
	}

}
