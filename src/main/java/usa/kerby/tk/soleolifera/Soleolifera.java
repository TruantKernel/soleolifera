package usa.kerby.tk.soleolifera;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Trevor Kerby
 * @since Jul 7, 2021
 */
public class Soleolifera {
	private final static String EMPTY = "";
	private final static String HYPHEN = "-";
	private final static Pattern NOT_ASCII = Pattern.compile("[^\\p{ASCII}]+");
	private final static Pattern IS_SPECIAL = Pattern.compile("[^\\w\\s]");
	private final static Pattern ARE_WHITE_SPACES = Pattern.compile("\\s+");
	private final static int HIGH_AVERAGE_CHARACTERS_PER_WORD = 7;
	private final static int LOW_AVERAGE_CHARACTERS_PER_WORD = 5;

	private final int lengthConstraint;
	private final float lengthWeight;
	private final int prefWordCount;
	private final float numericValueWeight;
	private final float capitalizationWeight;
	private final float properPhraseWeight;

	public static Soleolifera create() {
		return new Soleolifera(-1, -1, -1, -1, -1, -1);
	}

	public static Soleolifera createWithDefaultWeights() {
		return new Soleolifera(-1, 1, -1, 1, 1, 10);
	}

	public static Soleolifera createWithDefaultWeights(int lengthConstraint) {
		return new Soleolifera(lengthConstraint, 1, -1, 1, 1, 1);
	}

	public static Soleolifera createWithDefaultWeights(int lengthConstraint, int preferredWordCount) {
		return new Soleolifera(lengthConstraint, 1, preferredWordCount, 1, 1, 1);
	}

	public Soleolifera withLengthConstraint(int constraint) {
		return new Soleolifera(constraint, lengthWeight, prefWordCount, numericValueWeight, capitalizationWeight, properPhraseWeight);
	}

	public Soleolifera considerLength() {
		return new Soleolifera(lengthConstraint, 1, prefWordCount, numericValueWeight, capitalizationWeight, properPhraseWeight);
	}

	public Soleolifera considerLength(float weight) {
		return new Soleolifera(lengthConstraint, weight, prefWordCount, numericValueWeight, capitalizationWeight, properPhraseWeight);
	}

	public Soleolifera withPreferredWordCount(int preferredWordCount) {
		return new Soleolifera(lengthConstraint, lengthWeight, preferredWordCount, numericValueWeight, capitalizationWeight, properPhraseWeight);
	}

	public Soleolifera considerNumericValues() {
		return new Soleolifera(lengthConstraint, lengthWeight, prefWordCount, 1, capitalizationWeight, properPhraseWeight);
	}

	public Soleolifera considerNumericValues(float weight) {
		return new Soleolifera(lengthConstraint, lengthWeight, prefWordCount, weight, capitalizationWeight, properPhraseWeight);
	}

	public Soleolifera considerCapitalization() {
		return new Soleolifera(lengthConstraint, lengthWeight, prefWordCount, numericValueWeight, 1, properPhraseWeight);
	}

	public Soleolifera considerCapitalization(float weight) {
		return new Soleolifera(lengthConstraint, lengthWeight, prefWordCount, numericValueWeight, weight, properPhraseWeight);
	}

	public Soleolifera considerPhraseCapitalization() {
		return new Soleolifera(lengthConstraint, lengthWeight, prefWordCount, numericValueWeight, capitalizationWeight, 1);
	}

	public Soleolifera considerPhraseCapitalization(float weight) {
		return new Soleolifera(lengthConstraint, lengthWeight, prefWordCount, numericValueWeight, capitalizationWeight, weight);
	}

	private Soleolifera(int lengthConstraint, float lengthWeight, int preferredWordCount, float numericValueWeight, float capitalizationWeight,
			float properPhraseWeight) {
		this.lengthConstraint = lengthConstraint;
		this.lengthWeight = lengthWeight;
		this.prefWordCount = preferredWordCount;
		this.numericValueWeight = numericValueWeight;
		this.capitalizationWeight = capitalizationWeight;
		this.properPhraseWeight = properPhraseWeight;
	}

	public String generate(final String input) {
		String source = input.trim();
		if (source == null || source.isEmpty()) {
			return EMPTY;
		}
		source = normalize(source);
		source = removeSpecial(source);
		String[] tokens = tokenize(source);
		final float[] weights = weight(tokens);
		final int[] indices = generateSortedIndices(weights);
		int targetWordCount = (prefWordCount > 0) ? prefWordCount : guessUpperPreferredWordCount(lengthConstraint);
		String output = pick(tokens, indices, targetWordCount);
		int characterCount = output.length();
		if (lengthConstraint > 0 && characterCount > lengthConstraint) {
			int lowerAverageWordCount = guessLowerPreferredWordCount(characterCount);
			while (characterCount > lengthConstraint) {
				targetWordCount = (targetWordCount > lowerAverageWordCount) ? lowerAverageWordCount : targetWordCount - 1;
				output = pick(tokens, indices, targetWordCount);
				characterCount = output.length();
			}
		}
		return output.toLowerCase();
	}

	private float[] weight(String[] tokens) {
		float[] weights = new float[tokens.length];
		if (this.lengthWeight > 0) {
			weightByLength(tokens, weights, lengthWeight);
		}
		if (this.numericValueWeight > 0) {
			weightByIsValue(tokens, weights, numericValueWeight);
		}
		if (this.capitalizationWeight > 0) {
			weightByCapitalization(tokens, weights, capitalizationWeight, true, properPhraseWeight);
		}
		return weights;
	}

	private static final String normalize(final String input) {
		String output = Normalizer.normalize(input, Normalizer.Form.NFKD);
		return NOT_ASCII.matcher(output).replaceAll(EMPTY);
	}

	private static final String removeSpecial(final String input) {
		return IS_SPECIAL.matcher(input).replaceAll(EMPTY);
	}

	private static final String[] tokenize(final String input) {
		return ARE_WHITE_SPACES.split(input);
	}

	private static final int guessLowerPreferredWordCount(int characterConstraint) {
		return characterConstraint / HIGH_AVERAGE_CHARACTERS_PER_WORD;
	}

	private static final int guessUpperPreferredWordCount(int characterConstraint) {
		return characterConstraint / LOW_AVERAGE_CHARACTERS_PER_WORD;
	}

	private static final String pick(String[] tokens, int[] indices, int words) {
		words = (words >= tokens.length || words <= 0) ? tokens.length : words;
		int[] keep = Arrays.copyOfRange(indices, indices.length - words, indices.length);
		String res = "";
		int picked = 0;
		for (int i = 0; i < tokens.length; i++) {
			for (int j = 0; j < keep.length; j++) {
				if (i == keep[j]) {
					picked++;
					final String token = tokens[keep[j]];
					res += (picked < keep.length) ? token + HYPHEN : token;
				}
			}
		}
		return res;
	}

	private static final float[] weightByLength(String[] tokens, float[] weights, float multiplier) {
		for (int i = 0; i < tokens.length; i++) {
			weights[i] = (weights[i] + (tokens[i].length() * multiplier));
		}
		return weights;
	}

	private static final float[] weightByIsValue(String[] tokens, float[] weights, float multiplier) {
		for (int i = 0; i < tokens.length; i++) {
			int isUpper = (Character.isDigit(tokens[i].codePointAt(0))) ? 1 : 0;
			weights[i] = (weights[i] + (isUpper * multiplier));
		}
		return weights;
	}

	private static final float[] weightByCapitalization(String[] tokens, float[] weights, float multiplier, boolean considerProximity,
			float proximityMultiplier) {
		List<Integer> proximityIndices = null;
		if (considerProximity) {
			proximityIndices = new ArrayList<>();
		}
		for (int i = 0; i < tokens.length; i++) {
			final boolean isUpper = (Character.isUpperCase(tokens[i].codePointAt(0)));
			int occurances = isUpper ? 1 : 0;
			if (isUpper) {
				for (int j = 1; j < tokens[i].length(); j++) {
					if (Character.isUpperCase(tokens[i].codePointAt(j))) {
						occurances++;
					} else {
						break;
					}
				}
			}
			weights[i] = (weights[i] + (occurances * multiplier));
			if (considerProximity) {
				modifyWeightsForAssumedProperPhrase(weights, proximityIndices, i, isUpper, proximityMultiplier);
			}
		}
		return weights;
	}

	private static final void modifyWeightsForAssumedProperPhrase(float[] weights, List<Integer> indices, int currentIndex, boolean shouldStore,
			float multiplier) {
		if (shouldStore) {
			indices.add(currentIndex);
		} else if (!indices.isEmpty()) {
			for (int i : indices) {
				weights[i] = (weights[i] + (1 * multiplier));
			}
			indices.clear();
		}
	}

	private static final int[] generateSortedIndices(float[] weights) {
		float[] weightsCopy = weights.clone();
		int[] indices = generateSequentialIndices(weightsCopy.length);
		return generateInsertionSortedIndices(weights, indices);
	}

	private static final int[] generateInsertionSortedIndices(float[] weights, int[] indices) {
		for (int i = 1; i < weights.length; ++i) {
			float currentWeight = weights[i];
			int currentIndexValue = indices[i];
			int target = i - 1;
			while (target >= 0 && currentWeight < weights[target]) {
				weights[target + 1] = weights[target];
				indices[target + 1] = indices[target];
				target--;
			}
			weights[target + 1] = currentWeight;
			indices[target + 1] = currentIndexValue;
		}
		return indices;
	}

	private static final int[] generateSequentialIndices(int size) {
		int[] indices = new int[size];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = i;
		}
		return indices;
	}

}
