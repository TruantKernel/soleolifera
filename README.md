# Soleolifera

A library for configurable generation of human readable slugs to be used in URLs, filenames, or other fixed-length strings.

Soleolifera allows slug generation to be configured to produce slugs with: 
* a maximum character count
* a preferred word count
* the inclusion or exclusion of numeric values
* a preference for capitalized words
* a preference for capitalized phrases

## Examples

Generate a slug without constraints  

```java
		Soleolifera gen = Soleolifera.create();
		String slug = gen.generate(inputText);
```

Generate a slug with length constraints

```java
		Soleolifera gen = Soleolifera.create().withLengthConstraint(80);
		String slug = gen.generate(inputText);
```

Generate a slug with length constraint and default weights applied

```java
		Soleolifera gen = Soleolifera.createWithDefaultWeights().withLengthConstraint(80);
		String slug = gen.generate(inputText);
```